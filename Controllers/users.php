<?php
class Controllers_Users extends RestController {
	private $_con;
	private function DBconnect(){
		$this->_con=mysqli_connect("localhost","eladco","74618","eladco_angular");
	}
	public function get() {
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){
			if(!empty($this->request['params']['id'])) {
				$sql="SELECT * FROM users WHERE id=?";				
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']);
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result );
				$this->responseStatus = 200;
			} else {///////////
				$sql = "SELECT *FROM users";
				$usersSqlResult = mysqli_query ($this->_con, $sql);
				$users = [];
				while ($user = mysqli_fetch_array($usersSqlResult)){
					$users[] = ['name' => $user['name'], 'email'=> $user['email']];
				}
				$this->response = array('result' => $users); //בעצם יש לנו מערך שמכיל בתוכו איבר אחד של ריסולט והוא מכיל מערך של יוזרס
				$this->responseStatus = 200;
			}
				//$this->response = array('result' =>'Getting multiple users not yet implemented');
				/////////////
		}else{
			$this->response = array('result' =>'Wrong parameters for users' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
		$this->DBconnect();
			if(!empty($this->request['params']['payload'])) {
				$json = json_decode($this->request['params']['payload'],true); // אולי להוסיף []
					//var_dump($this->request['params']['payload'],true);
					//print_r($json);
					//echo $json[name];   
					//echo $json[email];
					$name = $json[name]; 
					$email = $json[email]; 
					//echo $name . '<br>'. $email;
					$sql = "INSERT INTO `users`(`id`, `name`, `email`) VALUES ('NULL','$name','$email')";
					$sqlInsert = mysqli_query ($this->_con, $sql);
					
					$lestId = "SELECT  * FROM  `users` ORDER BY  `users`.`id` DESC LIMIT 1";   /// this part is for showing the last id inserted
					$lastIdResult = mysqli_query ($this->_con, $lestId);
					$users = [];
					while ($user = mysqli_fetch_array($lastIdResult)){
					$id = ['id' => $user['id']];
					}

					echo "Hi, the last user inserted is: "
					."Name:"  .$name. " " 
					."Email:". $email ." Id: " ;
					$this->response = array('result' => $id);
					$this->responseStatus = 201;
				
				/* //////////////////////////////////////////// Instead of the part above, I could use the part under
					$last_id = $this->_con->insert_id;
					$this->response = $last_id;
					$this->responseStatus = 201;
				*/

			}
			else {
				$this->response = array('result' => 'no post implemented for users');
				$this->responseStatus = 201;					
			}

	}
	public function put() {
		$this->response = array('result' => 'no put implemented for users');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for users');
		$this->responseStatus = 200;
	}
}
